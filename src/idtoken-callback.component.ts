import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { EZCodeAdalService } from './ezcode-adal.service';

@Component({
    template: '<div>Please wait...</div>'
})
export class IdTokenCallbackComponent implements OnInit {
    constructor(private router: Router, private adalService: EZCodeAdalService) {

    }

    ngOnInit() {
        let redirectPath = '/admin/pump-archive';
        if(this.adalService.adalConfig && this.adalService.adalConfig.redirectPath) {
            redirectPath = this.adalService.adalConfig.redirectPath;
        }
        if (this.adalService.userInfo) {
            this.router.navigate([redirectPath]);
        }
    }
}